package validator

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	zh_translations "github.com/go-playground/validator/v10/translations/zh"
)

var (
	uni      *ut.UniversalTranslator
	validate *validator.Validate
	trans    ut.Translator
)

func Init() {
	//注册翻译器
	zh := zh.New()
	uni = ut.New(zh, zh)

	trans, _ = uni.GetTranslator("zh")

	//获取gin的校验器
	validate := binding.Validator.Engine().(*validator.Validate)

	validate.RegisterValidation("chineseMobile", chineseMobile)

	// 添加手机号验证规则
	validate.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("label"), ",", 2)[0]
		if name == "-" {
			return ""
		}

		return name
	})

	//注册翻译器
	zh_translations.RegisterDefaultTranslations(validate, trans)

	// 自定义验证规则翻译
	validate.RegisterTranslation("chineseMobile", trans, registerTranslator("chineseMobile", "{0}格式不正确"), translate)
}

//Translate 翻译错误信息
func Translate(err error) map[string][]string {

	var result = make(map[string][]string)
	if errors, ok := err.(validator.ValidationErrors); ok {
		for _, err := range errors {
			//fmt.Println(err.Tag(), "===", err.Field(), "===", err.ActualTag(), "===", err.StructField())
			//result[err.Field()] = append(result[err.Field()], err.Translate(trans))
			result[err.StructField()] = append(result[err.StructField()], err.Translate(trans))
		}
	}

	return result
}

// registerTranslator 为自定义字段添加翻译功能
func registerTranslator(tag string, msg string) validator.RegisterTranslationsFunc {
	return func(trans ut.Translator) error {
		if err := trans.Add(tag, msg, false); err != nil {
			return err
		}

		return nil
	}
}

// translate 自定义字段的翻译方法
func translate(trans ut.Translator, fe validator.FieldError) string {
	msg, err := trans.T(fe.Tag(), fe.Field())
	if err != nil {
		panic(fe.(error).Error())
	}

	return msg
}

// 国内手机号
//func chineseMobile(v *validator.Validate, topStruct reflect.Value, currentStructOrField reflect.Value, field reflect.Value, fieldType reflect.Type, fieldKind reflect.Kind, param string) bool {
func chineseMobile(fl validator.FieldLevel) bool {
	reg := regexp.MustCompile(`^1[3456789]\d{9}$`)

	return reg.MatchString(fl.Field().String())
}
