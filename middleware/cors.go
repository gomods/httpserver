// +----------------------------------------------------------------------
// |  
// |  
// | 
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/25 9:10 下午
// +----------------------------------------------------------------------
package middleware

import (
	"context"
	"time"

	"gitee.com/gomods/config"
	"gitee.com/gomods/logger"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// Config represents all available options for the middleware.
type Config struct {
	AllowAllOrigins bool `ini:"allowAllOrigins"`

	// AllowOrigins is a list of origins a cross-domain request can be executed from.
	// If the special "*" value is present in the list, all origins will be allowed.
	// Default value is []
	AllowOrigins []string `ini:"allowOrigins"`

	// AllowOriginFunc is a custom function to validate the origin. It take the origin
	// as argument and returns true if allowed or false otherwise. If this option is
	// set, the content of AllowOrigins is ignored.
	AllowOriginFunc func(origin string) bool

	// AllowMethods is a list of methods the client is allowed to use with
	// cross-domain requests. Default value is simple methods (GET and POST)
	AllowMethods []string `ini:"allowMethods"`

	// AllowHeaders is list of non simple headers the client is allowed to use with
	// cross-domain requests.
	AllowHeaders []string `ini:"allowHeaders"`

	// AllowCredentials indicates whether the request can include user credentials like
	// cookies, HTTP authentication or client side SSL certificates.
	AllowCredentials bool `ini:"allowCredentials"`

	// ExposedHeaders indicates which headers are safe to expose to the API of a CORS
	// API specification
	ExposeHeaders []string `ini:"exposeHeaders"`

	// MaxAge indicates how long (in seconds) the results of a preflight request
	// can be cached
	MaxAge time.Duration `ini:"maxAge"`

	// Allows to add origins like http://some-domain/*, https://api.* or http://some.*.subdomain.com
	AllowWildcard bool `ini:"allowWildcard"`

	// Allows usage of popular browser extensions schemas
	AllowBrowserExtensions bool `ini:"allowBrowserExtensions"`

	// Allows usage of WebSocket protocol
	AllowWebSockets bool `ini:"allowWebSockets"`

	// Allows usage of file:// schema (dangerous!) use it only when you 100% sure it's needed
	AllowFiles bool `ini:"allowFiles"`
}

func Cors() gin.HandlerFunc {
	var cfg Config

	err := config.ConfMapToStruct("HttpCors", &cfg)
	if err != nil {
		logger.Ex(context.Background(), "CorsMiddleware", "cors config to struct error", "error", err)
		return func(c *gin.Context) {}
	}

	corsConfig := cors.Config{
		AllowAllOrigins:        cfg.AllowAllOrigins,
		AllowOrigins:           cfg.AllowOrigins,
		AllowMethods:           cfg.AllowMethods,
		AllowHeaders:           cfg.AllowHeaders,
		AllowCredentials:       cfg.AllowCredentials,
		ExposeHeaders:          cfg.ExposeHeaders,
		MaxAge:                 cfg.MaxAge,
		AllowWildcard:          cfg.AllowWildcard,
		AllowBrowserExtensions: cfg.AllowBrowserExtensions,
		AllowWebSockets:        cfg.AllowWebSockets,
		AllowFiles:             cfg.AllowFiles,
	}

	return cors.New(corsConfig)
}
