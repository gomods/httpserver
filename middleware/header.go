package middleware

import "github.com/gin-gonic/gin"

func RequestHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		benchmark := c.GetHeader("Request-Type")
		if benchmark == "performance-testing" {
			c.Set("IS_BENCHMARK", "1")
		}
	}
}

func ResponseHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Server", "go-httpserver/"+hostname)
	}
}
