// +----------------------------------------------------------------------
// |  
// |  
// | 
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/10 5:10 下午
// +----------------------------------------------------------------------
package middleware

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http/httputil"
	"runtime"
	"strconv"
	"time"

	"gitee.com/gomods/logger"

	"github.com/gin-gonic/gin"
)

var (
	dunno     = []byte("???")
	centerDot = []byte("·")
	dot       = []byte(".")
	slash     = []byte("/")
)

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	if _, err := w.body.Write(b); err != nil {
		fmt.Printf("bodyLogWriter err:%v", err)
	}

	return w.ResponseWriter.Write(b)
}



//处理上下文相关
func Context() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set("log_id", strconv.FormatInt(logger.GenLoggerId(), 10))
		ctx.Set("hostname", hostname)
		ctx.Set("start", time.Now())

		traceId := ctx.GetHeader("traceId")
		if traceId == "" {
			traceId = ctx.GetHeader("traceid")
		}
		if traceId == "" {
			traceId = ctx.GetHeader("trace_id")
		}
		if traceId == "" {
			traceId = logger.GenTraceId()
		}
		ctx.Set("trace_id", traceId)

		rpcId := ctx.GetHeader("rpcId")
		if rpcId == "" {
			rpcId = ctx.GetHeader("rpcid")
		}
		if rpcId == "" {
			rpcId = ctx.GetHeader("rpc_id")
		}
		if rpcId == "" {
			rpcId = "1.0"
		} else {
			rpcId = rpcId + ".0"
		}
		ctx.Set("rpc_id", rpcId)
	}
}

func SkipLogInfo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set("SKIPLOG", "1")
	}
}

//recover middleware
func Recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				stack := stack(3)
				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				logger.Ex(context.Background(), "recovery", "http request panic recovered error", "request_url", c.Request.RequestURI, "http_request", string(httpRequest), "error", fmt.Sprintf("%v", err), "stacks", string(stack))
				fmt.Println("[recovery]", fmt.Sprintf("%s panic recovered:\n%s\n%s\n%s", time.Now().Format("2006/01/02 - 15:04:05"), string(httpRequest), err, string(stack)))
				c.AbortWithStatus(500)
			}
		}()
		c.Next()
	}
}

func stack(skip int) []byte {
	buf := new(bytes.Buffer) // the returned data
	// As we loop, we open files and read them. These variables record the currently
	// loaded file.
	var lines [][]byte
	var lastFile string
	for i := skip; ; i++ { // Skip the expected number of frames
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		// Print this much at least.  If we can't find the source, it won't show.
		fmt.Fprintf(buf, "%s:%d (0x%x)\n", file, line, pc)
		if file != lastFile {
			data, err := ioutil.ReadFile(file)
			if err != nil {
				continue
			}
			lines = bytes.Split(data, []byte{'\n'})
			lastFile = file
		}
		fmt.Fprintf(buf, "\t%s: %s\n", function(pc), source(lines, line))
	}
	return buf.Bytes()
}

func function(pc uintptr) []byte {
	fn := runtime.FuncForPC(pc)
	if fn == nil {
		return dunno
	}
	name := []byte(fn.Name())
	// The name includes the path name to the package, which is unnecessary
	// since the file name is already included.  Plus, it has center dots.
	// That is, we see
	//  runtime/debug.*T·ptrmethod
	// and want
	//  *T.ptrmethod
	// Also the package path might contains dot (e.g. code.google.com/...),
	// so first eliminate the path prefix
	if lastslash := bytes.LastIndex(name, slash); lastslash >= 0 {
		name = name[lastslash+1:]
	}
	if period := bytes.Index(name, dot); period >= 0 {
		name = name[period+1:]
	}
	name = bytes.Replace(name, centerDot, dot, -1)
	return name
}
func source(lines [][]byte, n int) []byte {
	n-- // in stack trace, lines are 1-indexed but our array is 0-indexed
	if n < 0 || n >= len(lines) {
		return dunno
	}
	return bytes.TrimSpace(lines[n])
}

func transferToContext(c *gin.Context) context.Context {
	ctx := context.Background()
	for k, v := range c.Keys {
		ctx = context.WithValue(ctx, k, v)
	}
	return ctx
}
