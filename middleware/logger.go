// +----------------------------------------------------------------------
// |
// |
// |
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/10 5:12 下午
// +----------------------------------------------------------------------
package middleware

import (
	"bytes"
	"io/ioutil"
	"os"
	"time"

	"gitee.com/gomods/logger"

	"github.com/gin-gonic/gin"
)

type CheckError func([]byte) bool

var (
	errCheck    CheckError
	hostname, _ = os.Hostname()
)

func CheckCode0(in []byte) bool {
	return (bytes.Index(in, []byte(`"code":0`)) < 0) && (bytes.Index(in, []byte(`"code": 0`)) < 0)
}

func Logger(fns ...CheckError) gin.HandlerFunc {
	if len(fns) > 0 {
		errCheck = fns[0]
	}

	return func(c *gin.Context) {
		blw := &bodyLogWriter{body: bytes.NewBuffer([]byte{}), ResponseWriter: c.Writer}
		c.Writer = blw

		// Start timer
		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery

		var body []byte
		if c.Request.Body != nil {
			body, _ = ioutil.ReadAll(c.Request.Body)
		}
		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body))

		// Process request
		c.Next()

		_, skip := c.Get("SKIPLOG")
		if skip {
			return
		}
		// Stop timer
		end := time.Now()
		latency := end.Sub(start)

		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()
		comment := c.Errors.ByType(gin.ErrorTypePrivate).String()

		if raw != "" {
			path = path + "?" + raw
		}

		buf := blw.body.Bytes()
		ctx := transferToContext(c)

		if errCheck != nil && errCheck(buf) {
			logger.Ex(ctx, "gin", "",
				"request_time", end.Format("2006/01/02 - 15:04:05"), "status_code", statusCode, "latency", latency.String(), "client_ip", clientIP, "method", method, "path", path, "comment", comment, "request_body", string(body), "response_body", string(buf),
			)
		} else {
			logger.Dx(ctx, "gin", "",
				"request_time", end.Format("2006/01/02 - 15:04:05"), "status_code", statusCode, "latency", latency.String(), "client_ip", clientIP, "method", method, "path", path, "comment", comment, "request_body", string(body), "response_body", string(buf),
			)
		}
	}
}
