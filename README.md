# HttpServer

HttpServer 基于 `gin` 封装，方便集成到 `udc` 项目中，无代码侵入，保留原汁原味的 `github.com/gin-gonic/gin`, 可同步更新新版本。

## 安装
```shell script
go get gitee.com/gomods/httpserver
```

## 配置
```ini
[Server]
;TCP与监听端口
addr = :8088
;运行模式. 可选debug/release
mode = release
```

## 初始化
```go
    
    import (
            "gitee.com/gomods/httpserver"
            "gitee.com/gomods/httpserver/bootstrap"
            "gitee.com/gomods/httpserver/middleware"
        )

	// 实例化 httpserver
	s := httpserver.NewServer()
	
    // 注册中间件 Logger，Recovery, Context, RequestHeader ...
    s.UseMiddleware(middleware.Logger(middleware.CheckCode0), middleware.Recovery(), middleware.Context(), middleware.RequestHeader())
	
    // 注册前置方法 初始化日志
    s.AddBeforeServerStartFunc(bootstrap.InitLogger("dev", "httpServerDemo", "gomods", "0.0.1"), s.InitConfig())
	
    // 注册后置方法
    s.AddAfterServerStopFunc(bootstrap.CloseLogger())

	// 注册路由
	app.RegisterRouter(s.GinEngine())
    
    // 启动服务并监听
	err := s.Serve()
	if err != nil {
		log.Printf("Server stop err:%v", err)
	} else {
		log.Printf("Server exit")
	}

```
> 默认路由注册放在 `app/service.go`

## 操作文档
https://gin-gonic.com/zh-cn/docs/

## 服务骨架示例
https://gitee.com/gomods/httpserver-demo
> 新项目可以直接克隆，替换名称并直接使用。