// +----------------------------------------------------------------------
// |
// |
// |
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/10 2:59 下午
// +----------------------------------------------------------------------

package httpserver

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitee.com/gomods/bootstrap"
	"gitee.com/gomods/config"
	"gitee.com/gomods/httpserver/validator"
	"gitee.com/gomods/logger"

	"github.com/gin-gonic/gin"
)

type Server struct {
	server      *http.Server
	beforeFuncs []bootstrap.BeforeServerStartFunc
	afterFuncs  []bootstrap.AfterServerStopFunc
	opts        ServerOptions
	exit        chan os.Signal
}

func NewServer() *Server {
	opts := DefaultOptions()
	s := new(Server)
	s.opts = opts

	s.InitGinValidatorChinese()

	gin.SetMode("release")

	handler := gin.New()
	server := &http.Server{
		Addr:         opts.Addr,
		Handler:      handler,
		ReadTimeout:  opts.ReadTimeout,
		WriteTimeout: opts.WriteTimeout,
		IdleTimeout:  opts.WriteTimeout,
	}
	s.exit = make(chan os.Signal, 2)
	s.server = server

	return s
}

func (s *Server) Serve() error {
	var err error
	for _, fn := range s.beforeFuncs {
		err = fn()
		if err != nil {
			return err
		}
	}

	signal.Notify(s.exit, os.Interrupt, syscall.SIGTERM)
	go s.waitShutdown()
	logger.Ix(context.Background(), "httpserver", fmt.Sprintf("httpserver start and serve:%s", s.server.Addr))
	err = s.server.ListenAndServe()
	if err != nil && err == http.ErrServerClosed {
		err = nil
	}

	for _, fn := range s.afterFuncs {
		fn()
	}

	return err
}

func (s *Server) waitShutdown() {
	<-s.exit
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	logger.Ix(ctx, "httpserver", "shutdown http server ...")

	err := s.server.Shutdown(ctx)
	if err != nil {
		logger.Ex(ctx, "httpserver", "shutdown http server error", "error", err.Error())
	}

	return
}

func (s *Server) Server() *http.Server {
	return s.server
}

func (s *Server) GinEngine() *gin.Engine {
	return s.server.Handler.(*gin.Engine)
}

func (s *Server) InitGinValidatorChinese() {
	validator.Init()
}

func (s *Server) UseMiddleware(middleware ...gin.HandlerFunc) {
	s.GinEngine().Use(middleware...)
}

func (s *Server) AddBeforeServerStartFunc(fns ...bootstrap.BeforeServerStartFunc) {
	for _, fn := range fns {
		s.beforeFuncs = append(s.beforeFuncs, fn)
	}
}

func (s *Server) AddAfterServerStopFunc(fns ...bootstrap.AfterServerStopFunc) {
	for _, fn := range fns {
		s.afterFuncs = append(s.afterFuncs, fn)
	}
}

func (s *Server) InitConfig(ops ...string) bootstrap.BeforeServerStartFunc {
	return func() error {
		sec := "HttpServer"
		if len(ops) > 0 && ops[0] != "" {
			sec = ops[0]
		}

		err := config.ConfMapToStruct(sec, &s.opts)
		if err != nil {
			return err
		}

		s.server.Addr = s.opts.Addr
		s.server.ReadTimeout = s.opts.ReadTimeout
		s.server.WriteTimeout = s.opts.WriteTimeout
		s.server.IdleTimeout = s.opts.IdleTimeout
		if s.opts.Mode != "" {
			gin.SetMode(s.opts.Mode)
		}

		return nil
	}
}
