module gitee.com/gomods/httpserver

go 1.15

require (
	gitee.com/gomods/bootstrap v0.0.1 // indirect
	gitee.com/gomods/config v0.0.3 // indirect
	gitee.com/gomods/logger v0.0.3 // indirect
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.8.1 // indirect

)
